# Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
<style>
mindmapDiagram {
    leafNode {
        BackgroundColor lightBlue
    }
    :depth(1) {
      BackGroundColor white
    }
	:depth(2) {
      BackGroundColor Linen
    }
	:depth(3) {
      BackGroundColor lightCyan
    }
    :depth(4) {
      BackGroundColor lightPink
    }
    :depth(5) {
      BackGroundColor lightPurple
    }
    :depth(6) {
      BackGroundColor lightGray
    }
}
</style>
* Programación Declarativa. 
**_ Orientaciones y pautas \n para el estudio \n(1998, 1999, 2001)
  
***_ Sus 
**** Ventajas
***** En general para cualquier tipo de app
***** Los Programas 
******_ son
******* cortos
******* Eficientes
*****_ Reduce 
****** La complegidad de los programas
*****_ Facilita 
****** La programación
******_ tiene
******* Ordenes directas
********_ Realizable con 
********* Métodos no conocidos 
*********_ en el momento de la 
********** Programación.
***** Optimización
******_ es
******* sencilla
********_  ya que 
********* la ejecución se gestiona 
**********_ mediante un 
*********** algoritmo
******_ tiene
******* Eficiencia del programa

**** Caracteristicas
***** Las tareas son dejadas \n al compilador
***** Eficientes
***** liberarse de detallar el control 
***** Más cercano 
******_ a la forma del
******* Pensamiento del progrmamador 
***** funciona
******_ a un nivel de
******* abstracción muy alto
***** Más faciles de depurar
***** no existe la asignación 
******_ ni el
******* cambio de estado en un programa
***** Las variables 
******_ son 
******* identificadores de valores 
********_ que no
********* cambian en toda la evaluación 
**********_ como constantes definidas
******_ Sólo existen 
******* valores 
******* expresiones matemáticas 
*******_ que devuelven 
******** nuevos valores 
*********_ a partir de 
********** los declarados
***** el progrmador no puede realizar declaraciones 
***** De un problema

**** Desventajas
***** Errores 
******_ Se pueden
******* Contener
******* introducir 
******_ y pueden
******* Perjudicar 
********_ la 
********* legibilidad del código
***** Exceso de detalles
****** Detalla cada paso
****** Toma en cuenta cada acción
***** casos de aplicación 
******_ los 
******* individuales 
******** se pueden considerar 
*********_ en la programación
********** pero únicamente de forma compleja
***** Cuello de botella
***** Obliga
******_ a tener más
******* detalles
********_ para 
********* Especificar un computo


*** Aplicaciones
****_ en 
***** Inteligencia Artificial
***** Sistemas expertos
***** Compiladores
***** Bases de datos deductivas
****_ Procesamiento de
***** Lenguaje Natural
**** Acceso a bases de datos desde páginas web

*** Lenguajes 
**** (DSLs)
*****_ son  
****** Lenguajes de dominio específico 
*******_ Significa
******** Lenguajes descriptivos para un propósito específico
********_  tales como 
********* HTML
********* CSS 
********* SQL.
**** Lenguajes híbridos
*****_ Un ejemplo son
****** los archivos “make” 
*******_ combinan la 
******** descripción de dependencias 
*********_ entre 
********** componentes 
**********_ con 
*********** instrucciones imperativas 
**********_ para
*********** compilar o instalar una aplicación
**** Los lenguajes lógicos
*****_ como 
****** Prolog.
**** Los lenguajes algebraicos
*****_ como 
****** Maude 
****** SQL.
**** Los lenguajes funcionales
*****_ como 
****** Haskell 
****** Erlang.
**** lenguaje hibrido 
*****_ como
****** Lisp 
******_ Permite hacer una
******* Programacion imperativa



*** Tipos de Programación

**** programación funcional
***** aplicacion de funciones 
******_ a unos 
******* datos 
***** Superior 
******_ a la 
******* Programación imperativa 
***** No hay disticion 
******_ entre 
******* datos y programas
***** razonamiento ecuacional 
***** utiles para hacer programas
****** una función sirve para hacer programas
****** especificar progrmas
****** Para hacer calculos requeridos en el programa 
***** Evaluacion peresoza
****** Los calculos no se realizan 
*******_ hasta que 
******** otro posterior no lo nececita
***** solo se calcula lo necesario
***** no se puede hacer en lenguaje imperativo

**** Programacion logica
***** predicados de primer orden 
***** relaciones entre objetos
*****_ Esta 
****** orientado a pautas
******* Axiomas
******* Leyes de inferencia 
*****_ Tiene
****** Relaciones entre objetos
****** Relacion factorial
***** no establece 
******_ un orden entre 
******* datos de entrada 
******* salida
***** opera a base de algoritmos de solucion 
***** conjunto de secuencia
***** no se especifica la secuencia de pasos 
****** solo se ingresan datos del problema 


@endmindmap
``` 

# Lenguaje de Programación Funcional (2015)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  node{
  lineColor purple
  }
  leafNode {
    LineColor white
    RoundCorner 0
    Shadowing 0.0
  }
  arrow {
    LineColor red
  }
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .purple {
    BackgroundColor lightPurple
  }
  .lila {
    BackgroundColor line
  }
  .gray {
    BackgroundColor lightGray
  }
  .cyan {
    BackgroundColor lightCyan
  }
}
</style>
* Programacion funcional 
** Lenguaje imperactivo <<green>>
*** las instrucciones <<green>>
****_ Se ejecutan <<green>>
***** unas tras otras <<green>>
**** Secuenciales <<green>>
****_ Excepto 
***** Bucles <<green>>
***** Estructuras de control <<green>>
****** Condicionales <<green>>
*** Programacion Orientada a Objetos <<green>>
**** Trozos de codigo <<green>>
**** Se modifican <<green>>
*****_ Por 
****** Series de intrucciones <<green>>

** Caracteristicas <<rose>>
*** Evaluación <<rose>>
**** Evaluación peresoza <<rose>>
**** Evaluación impaciente o estricta <<rose>>
***** Inconveniente con expeciones <<rose>>
**** Evaluacion estricta <<rose>>
***** Unica vez <<rose>>
**** Evaluacion Memoización <<rose>>
***** Buena memoria <<rose>>
***** Evaluacion peresoza <<rose>>
***** Secuencia costosa <<rose>>
***** Paso por necesidad <<rose>>
**** Evaluación no estricta <<rose>>
***** De afuera hacia adentro  
*****_ No nececita conocer
****** uno de los parametros
*** variables no modificables <<rose>>
**** Cuando se fija el dato <<rose>>
*****_ de una varible
****** El reultado sera el mismo 
*** las constantes pueden ir como funciones <<rose>>
*** todo son funciones <<rose>>
**** Parametros de orden superior <<rose>>
***** Pueden ir como parametros <<rose>>
***** Funciones como parametros \n de otras funciones

** Lenguajes <<lila>>
*** SCALA <<lila>>
**** multi-paradigma
****_ expresa 
***** patrones comunes
****_ Soporta funciones
***** anónimas
***** orden superior
***** anidadas
***** currificación
*** Haskell <<lila>>
**** estandarizado 
****_ en haskell
***** "una función es un ciudadano de primera clase"
****_ orígenes 
***** observaciones de Haskell Curry
**** características más interesantes
*****_ incluyen 
****** el soporte para tipos de datos
****** funciones recursivas
****** listas
****** tuplas
****** guardas 
****** calce de patrones.
*** Clojure <<lila>>
****_ de
***** Propósito general
**** maneja el dialecto de Lisp
****_ está 
***** enfocado en el paradigma funcional 
****_ con el fin de
***** eliminar la complejidad
******_ de la 
******* programación concurrente

** Ventajas <<gray>>
*** al no tener variales <<gray>>
****  Los programas dependen <<gray>>
*****_ del 
****** Parametro de entrada
*** transparencia referencial <<gray>>
**** los resultados de las funciones <<gray>>
*****_ son 
****** independientes
**** El orden no importa <<gray>>
*** Mejor Programacion <<gray>>
****_ ya que tiene
***** Altos niveles de abstracción
*** Rapidez de compilación 
*** Código más preciso y más corto <<gray>>
*** Optimizaciones <<gray>> 
****_ a partir de 
***** la utilización de funciones puras
*** Ventajas tecnicas <<cyan>>
**** Expresividad <<cyan>>
*****_ uso de 
****** los datos algebraicos en haskell <<cyan>>
**** Seguridad <<cyan>>
***** Inmutabilidad
**** Paralelismo
*****_ se pueden ralizar 
****** funciones en paralelo
*******_ siempre que estas cumplan
********  con ciertas reglas al momento de su ejecucion



** Desventajas <<purple>>
*** grandes cantidades de short-lived garbage
****_ Esto se debe  a la 
***** caracteristica de inmutabilidad
***** Los garbage collectors 
*****_ tienden a 
****** optimizar este aspecto
*** Menor eficiencia 
****_ en el uso de CPU 
**** Comparados con su contraparte imperativa
****_ Debido  
***** a muchas estructuras de variables mutables 
*** No apto para todas las tareas <<purple>>
****_ ya que 
***** No es adecuado para muchas \n recursiones de la misma pila 
*** Puede dar lugar a errores graves <<purple>>
*** Datos no modificables <<purple>>
****_ Como 
***** Variables <<purple>>

** historia y aplicación
*** Historia 
**** 1956 se empieza a desarrollar 
*****_ Por
****** El profesor John McCarthy
**** 1958 se empieza a programar
***** inteligencia artificial
*****_ Se crea
****** Lisp
*****_ En un
****** Computador 
*** ¿Dónde se aplica? <<cyan>>
**** Word <<cyan>>
****_ Juegos
***** Arcade <<cyan>>


@endmindmap
```
